from os import path, makedirs, walk, unlink
import subprocess
from shutil import rmtree
import re
from jinja2 import Environment, FileSystemLoader

workingDirPath = path.dirname(path.realpath(__file__))

pathTmp = path.join(workingDirPath, ".tmp")
pathRepo = path.join(pathTmp, "repo")
pathTmpSvg = path.join(pathTmp, "svg")
pathOutput = path.join(workingDirPath, "output")
pathDrawables = path.join(pathOutput, "drawables")

urlRepo = "https://codeberg.org/sinhala/akuru.git"

letters = [
    ["අ"],["ආ"],["ඇ"],["ඈ"],["ඉ"],["ඊ"],
    ["උ"],["ඌ"],["ඍ"], ["ඎ"],["ඏ"],["ඐ"],
    ["එ"],["ඒ"],["ඓ"],["ඔ"],["ඕ"],["ඖ"],
    ["අං"],["අඃ"],
    ["ක"],["ඛ"],["ග"],["ඝ"],["ඞ"],["ඟ"],
    ["ච"],["ඡ"],["ජ"],["ඣ"],["ඤ"],["ඦ"],
    ["ට"],["ඨ"],["ඩ"],["ඪ"],["ණ"],["ඬ"],
    ["ත"],["ථ"],["ද"],["ධ"],["න"],["ඳ"],
    ["ප"],["ඵ"],["බ"],["භ"],["ම"],["ඹ"],
    ["ය"],["ර"],["ල"],["ව"],
    ["ශ"],["ෂ"],["ස"],["හ"],["ළ"],
    ["ෆ"],["ඥ"],["ළු"]
]


def cloneOrPullRepo(repoPath, repoUrl):
    if path.isdir(repoPath):
        subprocess.run(["git", "-C", repoPath, "pull"])
    else:
        subprocess.run(["git", "clone", repoUrl, repoPath])


def updateRepos():
    cloneOrPullRepo(pathRepo, urlRepo)


def makeCleanFolder(folderPath):
    if path.exists(folderPath):
        for root, dirs, files in walk(folderPath):
            for filePath in files:
                unlink(path.join(root, filePath))
            for dirPath in dirs:
                rmtree(path.join(root, dirPath))
    else:
        makedirs(folderPath)


def prepareFolders():
    makeCleanFolder(pathTmpSvg)
    makeCleanFolder(pathDrawables)


svgRePatternViewboxWidth = r'viewBox=\"0 0 ([0-9]+\.?[0-9]*) 100\"'
svgRePatternMarkerStart = r'm([0-9]+\.?[0-9]*)'


def writeModifiedSVG(svgFilePath, dstFileName):
    with open(svgFilePath, "r") as readFile:
        svgContent = readFile.read()
        viewboxWidth = re.search(svgRePatternViewboxWidth, svgContent).group(1)
        newViewboxWidth = float(viewboxWidth) - 1
        markers = re.findall(svgRePatternMarkerStart, svgContent)
        for marker in markers:
            newMarker = round(float(marker) - 0.5, 2)
            svgContent = svgContent.replace(f'm{marker}', f'm{newMarker}')
        svgContent = svgContent.replace(f'viewBox=\"0 0 {viewboxWidth} 100\"', f'viewBox=\"0 0 {newViewboxWidth} 100\"')
        svgContent = re.sub(r'\s?fill=\"[a-z]+\"', "", svgContent)
        svgContent = re.sub(r'\s?stroke=\"#[0-9]+\"', "", svgContent)
        svgContent = re.sub(r'\s?stroke-linecap=\"[a-z]+\"', "", svgContent)
        svgContent = re.sub(r'\s?stroke-linejoin=\"[a-z]+\"', "", svgContent)
        
        with open(path.join(pathTmpSvg, dstFileName), "w") as writeFile:
            writeFile.write(svgContent)


def createSVGs():
    if path.exists(pathRepo):
        for root, dirs, files in walk(path.join(pathRepo, "අකුරු")):
            for svgFile in files:
                if svgFile.endswith("min.svg"):
                    svgFilePath = path.join(root, svgFile)
                    dstFileName = str(int((re.compile(r'(^[0-9]+)')).search(svgFile).group(1))) + ".svg" 
                    writeModifiedSVG(svgFilePath, dstFileName)

        # 'ළු' සඳහා
        svgFilePath = path.join(root, f'{pathRepo}/පිලි/06 - කෙටි පාපිල්ල/විශේෂ/59-ළ-min.svg')
        dstFileName = "62.svg"
        writeModifiedSVG(svgFilePath, dstFileName)


def getLetterSvgInfo(letterId):
    paths = []
    viewboxWidth = ""
    srcFile = path.join(pathTmpSvg, f'{letterId}.svg')
    with open(srcFile, "r") as readFile:
        svgContent = readFile.read()
        paths = []
        for i, svgPath in enumerate(re.findall(r'd="[a-zA-Z0-9\s\.-]+"', svgContent)):
            pathString = re.search(r'"([a-zA-Z0-9\s\.-]+)"', svgPath).group(1)
            for value in re.findall(r'[0-9]+\.[0-9]{3,}', pathString):
                pathString = pathString.replace(value, str(round(float(value), 2)))
            paths.append(pathString)
        viewboxWidth = re.search(svgRePatternViewboxWidth, svgContent).group(1)
    return paths, viewboxWidth


def createDrawable(letterId, paths, viewboxWidth):
    with open(path.join(pathDrawables, f'l{letterId}.xml'), "w") as writeFile:
        newPaths = []
        for i in range(len(paths)):
            svgPath = paths[i]
            markers = re.findall(svgRePatternMarkerStart, svgPath)
            for marker in markers:
                newMarker = round(float(marker) + ((100 - float(viewboxWidth)) / 2), 2)
                svgPath = svgPath.replace(f'm{marker}', f'm{newMarker}')
            newPaths.append(svgPath)
        writeFile.write(env.get_template('drawable.xml').render(paths=newPaths))


def buildAkuruPages():
        for i in range(len(letters)):
            letter = letters[i][0]
            letterId = i + 1
            paths, viewboxWidth = getLetterSvgInfo(letterId)
            letters[i].append(paths)
            letters[i].append(viewboxWidth)
            createDrawable(letterId, paths, viewboxWidth)
        with open(path.join(pathOutput, "Akuru.java"), "w") as writeFile:
            writeFile.write(env.get_template('akuru.java').render(letters=letters))


updateRepos()
prepareFolders()
createSVGs()
env = Environment(loader=FileSystemLoader('templates'), keep_trailing_newline=True)
buildAkuruPages()
