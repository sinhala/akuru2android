public enum Akuru {
    {% for i in range(letters|length) %}{% set letter = letters[i] %}L{{ i + 1 }}("{{ letter[0] }}", new String[]{
        {% set paths = letter[1] %}{% for j in range(paths|length) %}{% set path = paths[j] %}"{{ path }}"{% if j < paths|length - 1 %},{% endif %}{% endfor %}
    }, {{ letter[2] }}F){% if i < letters|length - 1 %},
    {% endif %}{% endfor %};

    String name; String[] paths; Float width;
    Akuru(String n, String[] p, Float w) { name = n; paths = p; width = w; }
    public Glyph getGlyph() { return new Glyph(paths, width); }
}
